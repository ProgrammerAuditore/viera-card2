import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
          appBar: AppBar(
            title: Text('Material App Bar'),
          ),
          body: ListView(
            children: [const buildCard(), const buildRoundedCard(), 
            const buildImageCard(), const buildImageInterationCard()],
          )),
    );
  }
}

class buildCard extends StatelessWidget {
  const buildCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          Text(
            'Aprende a programar',
            style: TextStyle(fontSize: 12.0),
          )
        ],
      ),
    );
  }
}

class buildRoundedCard extends StatelessWidget {
  const buildRoundedCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
      child: Container(
        padding: EdgeInsets.all(32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Titulo",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),),
            SizedBox(height: 4,),
            Text('Sub-titulo',style: TextStyle(fontSize: 20),),
          ],
        ),
      ),
      color: Colors.black26,
    );
  }
}

class buildImageCard extends StatelessWidget {
  const buildImageCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(260)),
      child: Stack(
        alignment: Alignment.center, 
        children: [
          Ink.image(image: NetworkImage('https://images.unsplash.com/photo-1534665482403-a909d0d97c67?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80'),
          height: 250,
          fit: BoxFit.cover)
        ],
        
        ),

    );
  }
}

class buildImageInterationCard extends StatelessWidget {
  const buildImageInterationCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Stack(
        alignment: Alignment.center, 
        children: [
          Ink.image(image: NetworkImage('https://images.unsplash.com/photo-1534665482403-a909d0d97c67?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80'),
          height: 440,
          fit: BoxFit.cover),
        ],
        
        
        
        ),
        

    );
  }
}